import React from "react";

export const FlexDemo = (props: FlexProps) => {
    const ImageStyle: React.CSSProperties = {
        width: 65,
        height: 65,
        borderRadius: 65
    }
    const Message: React.CSSProperties = {
        fontSize: 14,
    }
    const NameStyle: React.CSSProperties = {
        margin: 0
    }
    const DateStyle: React.CSSProperties = {
        margin: 0,
        color: 'grey',
        fontWeight: 'normal'
    }
    return (
        <Flex container width="300px" margin="32px auto">
            <img
                style={ImageStyle}
                src="https://discountdoorhardware.ca/wp-content/uploads/2018/06/profile-placeholder-3.jpg"
                alt="Profile"
            />
            <Flex flex={1} margin="0 0 0 16px">
                <Flex container justifyContent="space-between">
                    <h4 style={NameStyle}> John Doe </h4>
                    <h5 style={DateStyle}> 5 Hours Ago </h5>
                </Flex>
                <p style={Message}>
                    This message does not use the Flex component.
                </p>
            </Flex>

        </Flex>
    )
}

export const Flex = (props: FlexProps) => {
    return (
        <div
            className={props.className}
            style={{
                display: props.container ? 'flex' : 'block',
                justifyContent: props.justifyContent || 'flex-start',
                flexDirection: props.flexDirection || 'row',
                flexGrow: props.flexGrow || 0,
                flexBasis: props.flexBasis || 'auto',
                flexShrink: props.flexShrink || 1,
                flexWrap: props.flexWrap || 'nowrap',
                flex: props.flex || '0 1 auto',
                alignItems: props.alignItems || 'stretch',
                margin: props.margin || '0',
                padding: props.padding || '0',
                width: props.width || 'auto',
                height: props.height || 'auto',
                maxWidth: props.maxWidth || 'none'
            }}
        >
            {props.children}
        </div>
    )
}

interface FlexProps {
    children?: any;
    maxWidth?: "-moz-initial" | "inherit" | "initial" | "revert" | "unset" | "-moz-max-content" | "-moz-min-content" | "-webkit-max-content" | "-webkit-min-content" | "auto" | "intrinsic" | "max-content" | "min-content" | string | number;
    height?: "-moz-initial" | "inherit" | "initial" | "revert" | "unset" | "-moz-max-content" | "-moz-min-content" | "auto" | "max-content" | "min-content" | string | number;
    width?: "-moz-initial" | "inherit" | "initial" | "revert" | "unset" | "-moz-max-content" | "-moz-min-content" | "-webkit-max-content" | "auto" | "intrinsic" | "max-content" | "min-content" | "min-intrinsic" | string | number;
    padding?: "-moz-initial" | "inherit" | "initial" | "revert" | "unset" | string | number;
    margin?: "-moz-initial" | "inherit" | "initial" | "revert" | "unset" | "auto" | string | number;
    alignItems?: "-moz-initial" | "inherit" | "initial" | "revert" | "unset" | "center" | "end" | "flex-end" | "flex-start" | "self-end" | "self-start" | "start" | "baseline" | "normal" | "stretch" | string;
    flex?: "-moz-initial" | "inherit" | "initial" | "revert" | "unset" | "auto" | "content" | "max-content" | "min-content" | "none" | string | number;
    flexWrap?: "-moz-initial" | "inherit" | "initial" | "revert" | "unset" | "nowrap" | "wrap" | "wrap-reverse";
    flexShrink?: "-moz-initial" | "inherit" | "initial" | "revert" | "unset" | number;
    flexBasis?: "-moz-initial" | "inherit" | "initial" | "revert" | "unset" | "-moz-max-content" | "-moz-min-content" | "-webkit-auto" | "auto" | "content" | "max-content" | "min-content" | string | number;
    flexGrow?: "-moz-initial" | "inherit" | "initial" | "revert" | "unset" | number;
    flexDirection?: "-moz-initial" | "inherit" | "initial" | "revert" | "unset" | "column" | "column-reverse" | "row" | "row-reverse";
    justifyContent?: "-moz-initial" | "inherit" | "initial" | "revert" | "unset" | "space-around" | "space-between" | "space-evenly" | "stretch" | "center" | "end" | "flex-end" | "flex-start" | "start" | "left" | "normal" | "right" | string;
    container?: any// container?: "-moz-initial" | "inherit" | "initial" | "revert" | "unset" | "block" | "inline" | "run-in" | "-ms-flexbox" | "-ms-grid" | "-webkit-flex" | "flex" | "flow" | "flow-root" | "grid" | "ruby" | "table" | "ruby-base" | "ruby-base-container" | "ruby-text" | "ruby-text-container" | "table-caption" | "table-cell" | "table-column" | "table-column-group" | "table-footer-group" | "table-header-group" | "table-row" | "table-row-group" | "-ms-inline-flexbox" | "-ms-inline-grid" | "-webkit-inline-flex" | "inline-block" | "inline-flex" | "inline-grid" | "inline-list-item" | "inline-table" | "contents" | "list-item" | "none" | string | boolean;
    className?: string;

}