import * as React from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Navigation from "./nav/Navigation";
import CustomizedTables from "./table/Table";
import WordOfTheDay from "./WordOfTheDay";
import Entity from "./table/Entity";

export default function App() {
    return (
        <Router>
            <div className="App">
                <Navigation/>
                <Switch>
                    <Route exact path="/">
                        <WordOfTheDay/>
                    </Route>
                    <Route exact path="/2019">
                        <CustomizedTables/>
                    </Route>
                    <Route path="/results/:nr">
                        <Entity/>
                    </Route>
                </Switch>
            </div>
        </Router>
    );
}

