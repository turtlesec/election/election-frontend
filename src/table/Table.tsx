import React, {useEffect, useState} from 'react';
import {withStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import 'fontsource-roboto';
import {Flex} from "../Flex";
import TypedFetch from "../TypedFetch";
import ApiResponse, {Items} from "../ApiResponse";
import {Link} from "react-router-dom";
import {REACT_APP_API_URL} from "../GetEnvVars";
import {useMediaQuery} from "react-responsive";



export default function CustomizedTables() {

    const StyledTableCell = withStyles((theme) => ({
        head: {
            backgroundColor: theme.palette.common.black,
            color: theme.palette.common.white,
        },
        body: {
            fontSize: 14,
        },
    }))(TableCell);

    const StyledTableRow = withStyles((theme) => ({
        root: {
            '&:nth-of-type(odd)': {
                backgroundColor: theme.palette.action.hover,
            },
        },
    }))(TableRow);
    const [rows, setRows] = useState<Items[]>([]);
    useEffect(() => {

        const schema = 'http://';
        const url = REACT_APP_API_URL || 'localhost:8080';
        const paths = (name:String) => ({
            getAllMunicipalities: schema + url + `/api/counties/${name}/municipalities`
        });
        const getData = async () => {
            const data = await TypedFetch<ApiResponse>(paths("innlandet").getAllMunicipalities);
            setRows(data.items)
        };
        getData()
    }, []);

    const isTabletOrMobile = useMediaQuery({query: '(max-width: 1224px)'});

    return (
        <Flex container className="myTable" width={isTabletOrMobile?"100%":"80%"} margin="0 auto">
            <TableContainer component={Paper} className="myTable">
                <Table className="municipalityTable" aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            <StyledTableCell>Municipality</StyledTableCell>
                            <StyledTableCell>County</StyledTableCell>
                            <StyledTableCell>Link</StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.map((row) => (
                            <StyledTableRow key={row.name}>
                                <StyledTableCell component="th" scope="row">{row.originalName}</StyledTableCell>
                                <StyledTableCell>{row.parentOriginalName}</StyledTableCell>
                                {/*<StyledTableCell><a href={"http://161.35.156.89:8081" + row.link}>{row.link}</a></StyledTableCell>*/}
                                <StyledTableCell><Link to={row.link.replace("/api", "")}>{row.link.replace("/api", "")}</Link></StyledTableCell>
                            </StyledTableRow>
                        ))}
                    </TableBody>

                </Table>
            </TableContainer>
        </Flex>
    );
}
