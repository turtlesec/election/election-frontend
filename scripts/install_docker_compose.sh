#!/usr/bin/env bash
echo "Checking if docker-compose is installed..."

if ! command -v docker-compose &>/dev/null; then
  echo "Docker-compose not installed. Proceeding to installation"
  sudo curl -L "https://github.com/docker/compose/releases/download/1.27.3/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
  sudo chmod +x /usr/local/bin/docker-compose
else
  echo "Docker-compose already installed"
fi
