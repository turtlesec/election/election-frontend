import React, {useEffect, useState} from 'react';
import {withStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import 'fontsource-roboto';
import {Flex} from "../Flex";
import TypedFetch from "../TypedFetch";
import {useParams} from 'react-router-dom'
import {REACT_APP_API_URL} from "../GetEnvVars";
import {useMediaQuery} from "react-responsive";


export default function Entity() {
    interface Response {
        items: Items[]
    }

    interface Items {
        name?: string,
        nr?: string,
        electorates?: number,
        originalName?: string,
        votes?: Votes,
        participation?: Participation
    }

    interface Votes {
        totalt?: number
        earlyVotes?: number
        votesTotalRejected?: number
        votesEarlyVotesRejected?: number
    }

    interface Participation {
        prosent?: number
    }

    let {nr} = useParams<any>()

    const StyledTableCell = withStyles((theme) => ({
        head: {
            backgroundColor: theme.palette.common.black,
            color: theme.palette.common.white,
        },
        body: {
            fontSize: 14,
        },
    }))(TableCell);

    const StyledTableRow = withStyles((theme) => ({
        root: {
            '&:nth-of-type(odd)': {
                backgroundColor: theme.palette.action.hover,
            },
        },
    }))(TableRow);

    const [row, setRows] = useState<Items>({})
    useEffect(() => {
        const schema = 'http://'
        const url = REACT_APP_API_URL || 'localhost:8080';
        const paths = () => ({
            getResultsFromEntity: schema + url + `/api/results/${nr}`
        });
        const getData = async () => {
            const data = await TypedFetch<Response>(paths().getResultsFromEntity)
            setRows(data.items[0])
        };
        getData()
    }, [nr])

    function tableRow(name: string, value: string | number | undefined) {
        return (
            <StyledTableRow>
                <StyledTableCell component="th" scope="row">{name}</StyledTableCell>
                <StyledTableCell>{value}</StyledTableCell>
            </StyledTableRow>
        )
    }

    function toLocaleString(param: string |number| undefined) {
        if (param === undefined) {
            return "0".toLocaleString()
        } else {
            return param.toLocaleString()
        }
    }

    const isTabletOrMobile = useMediaQuery({query: '(max-width: 1224px)'});

    return (
        <Flex container className="myTable" width={isTabletOrMobile?"100%":"80%"} margin="0 auto">
            <TableContainer component={Paper} className="myTable">
                <Table className="municipalityTable" aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            <StyledTableCell>Name</StyledTableCell>
                            <StyledTableCell>{row.originalName}</StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {tableRow("Number", toLocaleString(row.nr))}
                        {tableRow("Votes", toLocaleString(row.votes?.totalt))}
                        {tableRow("Electorates", toLocaleString(row.electorates))}
                        {tableRow("Participation", toLocaleString(row.participation?.prosent))}
                    </TableBody>

                </Table>
            </TableContainer>
        </Flex>
    );
}
