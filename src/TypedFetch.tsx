const props = {
    method: "get",
    headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json"
    })
};

export default function typedFetch <T>(request: RequestInfo): Promise<T> {
    return new Promise(resolve => {
        fetch(request, props)
            .then(response => response.json())
            .then(body => {
                resolve(body);
            });
    });
};