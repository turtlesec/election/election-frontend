FROM node:13.12.0-alpine

RUN apk update && apk upgrade && npm install -g serve

WORKDIR /app

COPY ./scripts/replace_env_vars.sh ./
RUN chmod 755 ./replace_env_vars.sh
COPY ./build ./

#CMD ["serve", "-s", "/app"]
CMD ./replace_env_vars.sh && serve -s .