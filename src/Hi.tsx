import {Flex} from "./Flex";
import React from "react";

export default function Hi() {
    return(
        <Flex>
            <h1>Hi, {process.env.REACT_APP_NOT_SECRET_CODE}</h1>
        </Flex>
    )
}