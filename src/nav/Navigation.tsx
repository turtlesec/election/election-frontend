import React from "react";
import {Flex} from "../Flex";
import * as CSS from 'csstype';
import {useMediaQuery} from 'react-responsive';

const navProps: CSS.Properties = {
    display: 'flex',
    justifyContent: 'space-around',
    width: '100%',
    marginTop: '1rem',
}

const navPropsMobile: CSS.Properties = {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
    width: '100%',
    marginTop: '1rem',
}

const navStyle: CSS.Properties = {
    listStyle: 'none',
    display: 'flex',
    textTransform: 'uppercase',
    textDecoration: 'none',
    color: 'white'
}

const navStyleMobile: CSS.Properties = {
    listStyle: 'none',
    display: 'flex',
    textAlign: 'center',
    flexDirection: 'column',
    textTransform: 'uppercase',
    textDecoration: 'none',
    color: 'white',
    margin: '0 0 0 0',
    padding: '0 0 0 0'

}

const brandStyle: CSS.Properties = {
    textTransform: 'uppercase',
    color: '#303137',
    justifyContent: 'space-around'
}

const spanStyle: CSS.Properties = {
    color: 'red'
}

const liStyle: CSS.Properties = {
    margin: 'auto 1rem',
    textDecoration: 'none',
    color: '#303137'
}

export default function Navigation() {
    const isTabletOrMobile = useMediaQuery({query: '(max-width: 1224px)'});
    return (
        <Flex container className="flex">
            <nav className="navbar" style={isTabletOrMobile ? navPropsMobile : navProps}>
                <Flex container className="brand">
                    <p style={brandStyle}>Turlesec <span style={spanStyle}>Elections</span></p></Flex>
                {/*<Flex container>*/}
                <ul className="topnav" style={isTabletOrMobile ? navStyleMobile : navStyle}>
                    <li><a style={liStyle} href="/">Home</a></li>
                    <li><a style={liStyle} href="/2019">Municipalities</a></li>
                    <li><a style={liStyle} href="/">About us</a></li>
                    <li><a style={liStyle} href="/">Contact</a></li>
                </ul>
                {/*</Flex>*/}
            </nav>
        </Flex>
    )
}