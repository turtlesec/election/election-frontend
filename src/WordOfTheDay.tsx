import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {Flex} from "./Flex";

const useStyles = makeStyles({
    root: {
        minWidth: 275,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
});

export default function WordOfTheDay() {
    const classes = useStyles();
    const bull = <span className={classes.bullet}>•</span>;

    return (
        <Flex container margin="8rem auto" justifyContent="center">
            <Card className={classes.root} variant="outlined">
                <CardContent>
                    <Typography className={classes.title} color="textSecondary" gutterBottom>
                        Word of the Day
                    </Typography>
                    <Typography variant="h5" component="h2">
                        elec{bull}tion
                    </Typography>
                    <Typography className={classes.pos} color="textSecondary">
                        noun
                    </Typography>
                    <Typography variant="body2" component="p">
                        an act or process of electing
                        <br/>
                        {'"the election of a new governor"'}
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button size="small">Learn More</Button>
                </CardActions>
            </Card>
        </Flex>
    );
}