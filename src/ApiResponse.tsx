export default interface ApiResponse {
    items : Items[]
}

export interface Items{
    link: string
    county: string
    name: string
    nr: string
    originalName: string
    parentOriginalName: string
}
