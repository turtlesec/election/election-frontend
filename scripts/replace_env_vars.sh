#!/bin/sh -e
echo "Script replacing dynamically environment variables in all files"
echo "Replacing environment variables"
replace() {
find ./ -name '*.js' | xargs sed -i "s|{$1}|$2|g"
find ./ -name '*.html' | xargs sed -i "s|{$1}|$2|g"
}
# replace all the environments
replace REACT_APP_API_URL "${REACT_APP_API_URL}"
echo "Done"