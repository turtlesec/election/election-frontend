// envVars.js
export const REACT_APP_API_URL = getEnvVar('{REACT_APP_API_URL}')
/** When in develop mode, read env vars normally, when in production output a
 * special string that will be replaced by script
 * @return {string} the env var value
 * @param envVarStr
 */
function getEnvVar(envVarStr: string) {
// is true when running: npm run build
    const isProd = process.env.NODE_ENV === 'production'
    const envVar = envVarStr.replace(/[{}]/g, '')
    return isProd ? envVarStr : process.env[envVar]
}