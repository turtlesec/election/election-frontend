#!/usr/bin/env bash
echo "Checking if docker is installed..."

if ! command -v docker &>/dev/null; then
  echo "Docker not installed. Installing latest version"
  curl -fsSL https://get.docker.com -o get-docker.sh
  sudo sh get-docker.sh
else
  echo "Docker already installed"
fi
